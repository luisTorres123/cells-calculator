import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-calculator.js';

suite('<cells-calculator>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-calculator></cells-calculator>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





