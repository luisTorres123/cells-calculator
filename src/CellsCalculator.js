import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsCalculator-styles.js';

import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js'
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
/**
This component ...

Example:

```html
<cells-calculator></cells-calculator>
```

##styling-doc

@customElement cells-calculator
@polymer
@LitElement
@demo demo/index.html
*/
export class CellsCalculator extends LitElement {
  static get is() {
    return 'cells-calculator';
  }

  // Declare properties
  static get properties() {
    return {
      leftElements: { type: String },
      rightElements: { type: String },
      lastExpression: { type: String },
      expression: { type: String }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.leftElements = '7894561230.=';
    this.rightElements = '<÷x-+';
    this.lastExpression = '';
    this.expression = '';
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-calculator-shared-styles')
    ]
  }

  // Define a template
  render() {
    return html`
      <bbva-web-form-text id="operation_result" value="${this.expression}" label="${this.lastExpression}" label-out="" size="L" readonly></bbva-web-form-text>
      <div class="elements">
        <div class="left-elements">
          ${this._setButtons(this.leftElements)}
        </div>
        <div class="right-elements">
          ${this._setButtons(this.rightElements)}
        </div>
      </div>
    `;
  }

  _setButtons(elements) {
    return elements.split('').map(element => this._showButton(element));
  }

  _showButton(el) {
    return html`
      <bbva-web-button-default size="l" @click="${() => this._addElement(el)}">${el}</bbva-web-button-default>
    `
  }

  _addElement(el) {
    switch (el) {
      case '=':
        this._executeExpression();
        break;
      case 'C':
        this.lastExpression = '';
        this.expression = '';
        this.rightElements = '<÷x-+';
        break;
      case '<':
        this.expression = this.expression.substr(0, this.expression.length-1);
        break;
      default:
        this.expression += el;
        break;
    }
  }

  _executeExpression() {
    this.lastExpression = this.shadowRoot.getElementById('operation_result').value;
    this._executePlusExpression();
    this.rightElements = 'C÷x-+';
  }

  _executePlusExpression() {
    let exp = this.expression.split('+').map(number => parseFloat(this._executeMinusExpression(number)));
    let result = exp.reduce((total, number) => total + number, 0);
    this.expression = result.toString();
  }

  _executeMinusExpression(subExp) {
    let exp = subExp.split('-').map(number => parseFloat(this._executeMultExpression(number)));
    let result = exp.slice(1).reduce((total, number) => total - number, exp[0]);
    return result;
  }

  _executeMultExpression(subExp) {
    let exp = subExp.split('x').map(number => parseFloat(this._executeDivExpression(number)));
    let result = exp.reduce((total, number) => total * number, 1.0);
    return result;
  }

  _executeDivExpression(subExp) {
    let exp = subExp.split('÷').map(number => parseFloat(number));
    let result = exp.slice(1).reduce((total, number) => number === 0 ? this._showError('zero') : total / number, exp[0]);
    return result;
  }

  _showError(er) {
    this.rightElements = 'C÷x-+';
    switch (er) {
      case 'zero':
        this.expression = 'No se puede dividir por 0';
        throw new Error("divide by zero");
    }
  }
  
}
